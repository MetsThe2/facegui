#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <iostream>
#include <iomanip>
#include <vector>
#include <numeric>
#include <string>
#include <map>
#include "wrapper.h"
#include "caffe/classification.hpp"

cv::Rect detectFace(cv::Mat &image, cv::CascadeClassifier *cascade, double scale) {
    cv::Mat grayscale, imageScaled;
    std::vector<cv::Rect> faces;
    cv::cvtColor(image, grayscale, cv::COLOR_BGR2GRAY);
    cv::resize(grayscale, imageScaled, cv::Size(), scale, scale, cv::INTER_LINEAR);
    cv::equalizeHist(imageScaled, imageScaled);

    const int faceFlags = CV_HAAR_SCALE_IMAGE | CV_HAAR_FIND_BIGGEST_OBJECT;
    cascade->detectMultiScale(imageScaled, faces, 1.2, 2, faceFlags, cv::Size(90, 90), cv::Size(1000, 1000));
    
    cv::Rect face0 = cv::Rect(0, 0, image.cols, image.rows);
    if (faces.size() > 0) {
        // Rescale detection
        const double rescale = (1.0 / scale);
        const int face0_rescale_x = cvRound(faces[0].x * rescale);
        const int face0_rescale_y = cvRound(faces[0].y * rescale);
        const int face0_rescale_width = cvRound(faces[0].width * rescale);
        const int face0_rescale_height = cvRound(faces[0].height * rescale);
        face0 = cv::Rect(face0_rescale_x, face0_rescale_y, face0_rescale_width, face0_rescale_height);

        // Add margin (OpenCV detection similar to CCV library detection?)
        const int margin = cvRound(0.15 * face0.width);
        const int face0_margin_x = std::max(0, face0.x - margin);
        const int face0_margin_y = std::max(0, face0.y - margin);
        const int face0_margin_width = std::min(image.cols, face0.x + face0.width + 2*margin) - face0.x;
        const int face0_margin_height = std::min(image.rows, face0.y + face0.height + 2*margin) - face0.y;
        face0 = cv::Rect(face0_margin_x, face0_margin_y, face0_margin_width, face0_margin_height);
    }
    return face0;
}

int init(void **cascade_ptr_ptr, void **classifier_ptr_ptr, char ***label_names_ptr) {
    cv::CascadeClassifier *cascade = new cv::CascadeClassifier;
    *cascade_ptr_ptr = cascade;
    if (!cascade->load("facedetect/Classifiers/haarcascade_frontalface_alt.xml")) {
        std::cerr << "ERROR: Could not load classifier cascade" << std::endl;
        return -1;
    }

    const std::string model_file   = "facedetect/Caffe/model.prototxt";
    const std::string trained_file = "facedetect/Caffe/trained.caffemodel";
    const std::string mean_file    = "facedetect/Caffe/mean.binaryproto";
    const std::string label_file   = "facedetect/Caffe/labels.txt";
    Classifier *classifier = new Classifier(model_file, trained_file, mean_file, label_file);
    *classifier_ptr_ptr = classifier;

    const std::vector<std::string> name_vec = classifier->GetLabels();
    const int label_num = (int)name_vec.size();
    char **label_names = *label_names_ptr = new char*[label_num];
    for (size_t i = 0; i < name_vec.size(); ++i) { // Copy names so they won't be deallocated.
        label_names[i] = new char[name_vec[i].size() + 1];
        std::copy(name_vec[i].begin(), name_vec[i].end(), label_names[i]);
        label_names[i][name_vec[i].size()] = '\0';
    }
    return label_num;
}

void classify(int *output_bbox, int *output_labels, float *output_prs, int label_num,
              void *cascade_ptr, double scale, void *classifier_ptr,
              int frame_width, int frame_height, unsigned char *frame_data) {
    const int type = CV_8UC3; // TODO: Correct? Should this be a function parameter?
    cv::Mat frame(frame_height, frame_width, type, frame_data); // TODO: step == AUTO_STEP ?
    
    cv::CascadeClassifier *cascade = static_cast<cv::CascadeClassifier*>(cascade_ptr);
    const cv::Rect face0 = detectFace(frame, cascade, scale);
    output_bbox[0] = face0.x,     output_bbox[1] = face0.y;
    output_bbox[2] = face0.width, output_bbox[3] = face0.height;

    if ((face0.width != frame.cols) && (face0.height != frame.rows)) {
        Classifier *classifier = static_cast<Classifier*>(classifier_ptr);
        const cv::Mat frame_face0 = frame(face0);
        const std::vector<std::pair<int, float> > idx_predictions =
            classifier->ClassifyIdx(frame_face0, label_num);
        for (int i = 0; i < idx_predictions.size(); ++i) {
            if (i >= label_num) {
                std::cerr << "WARNING: More predictions than labels" << std::endl;
                break;
            }
            output_labels[i] = idx_predictions[i].first;
            output_prs[i] = idx_predictions[i].second;
        }
    } else {
        output_labels[0] = -1; // Labels are unknown.
    }
}

void fin(void *cascade_ptr, void *classifier_ptr, char **label_names, int label_num) {
    for (int i = 0; i < label_num; ++i) delete[] label_names[i];
    delete[] label_names;
    cv::CascadeClassifier *cascade = static_cast<cv::CascadeClassifier*>(cascade_ptr);
    delete cascade;
    Classifier *classifier = static_cast<Classifier*>(classifier_ptr);
    delete classifier;
}