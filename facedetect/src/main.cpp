﻿//////////////////////////////////////////////////////////////////////////
// MAIN
// -----------------------------------------------------------------------
/**
 * Programa que detecta caras y reconoce sus emociones.
 * 
 * @author  Miguel Vinyas <miguel@cvc.uab.es>
 *
 *          Centro de Vision por Computador
 *          Edifici O - Universitat Autonoma de Barcelona.
 *          08193 Cerdanyola del Valles, Barcelona, (SPAIN).
 *          Tel. +(34) 93.581.18.28
 *          Fax. +(34) 93.581.16.70
 *
 * @version 1.0 (7 de Marzo de 2017)
 */
//////////////////////////////////////////////////////////////////////////
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <iostream>
#include <iomanip>
#include <vector>
#include <numeric>
#include <string>
#include <map>
#include "caffe/classification.hpp"


//////////////////////////////////////////////////////////////////////////
#define ARG(str)                    "-"str"="
#define TAG_PARAM_INPUT             "input"
#define TAG_PARAM_OUTPUT            "output"


//////////////////////////////////////////////////////////////////////////
#define NUM_LABELS                  8
#define NUM_SMOOTH_LABELS           30
#define FPS_VIDEO_OUTPUT            30


//////////////////////////////////////////////////////////////////////
template<class T>
struct greater_second : std::binary_function<T, T, bool>
{
	inline bool operator()(const T &lhs, const T &rhs) const
	{
		return (lhs.second > rhs.second);
	}
};


//////////////////////////////////////////////////////////////////////////
cv::Rect        detectFace(cv::Mat &image, cv::CascadeClassifier &cascade, cv::CascadeClassifier &nestedCascade, double scale=1.0);
std::string     smoothLabeling(const std::list<std::string> &listLabels, const int size=5);



// =======================================================================
// FUNCTION
// =======================================================================

// =======================================================================
// COMMAND_LINE_PARSER
// -----------------------------------------------------------------------
/**
 * Funcion que obtiene los parametros del programa.
 */
// =======================================================================
void commandLineParser(int argc, char *argv[], std::map<std::string, std::string> &parameters)
{
	// Parse parameters
	for (int i=1; i<argc; i++)
	{
		const std::string param = std::string(argv[i]);

		if (param.substr(0, strlen(ARG(TAG_PARAM_INPUT))) == ARG(TAG_PARAM_INPUT))
			parameters[TAG_PARAM_INPUT] = param.substr(strlen(ARG(TAG_PARAM_INPUT)), param.size());

		if (param.substr(0, strlen(ARG(TAG_PARAM_OUTPUT))) == ARG(TAG_PARAM_OUTPUT))
			parameters[TAG_PARAM_OUTPUT] = param.substr(strlen(ARG(TAG_PARAM_OUTPUT)), param.size());
	}
}
// commandLineParser


// =======================================================================
// DETECT_FACE
// -----------------------------------------------------------------------
/**
 * Funcion que detecta una cara en una imagen.
 */
// =======================================================================
cv::Rect detectFace(cv::Mat &image, cv::CascadeClassifier &cascade, cv::CascadeClassifier &nestedCascade, double scale)
{
	cv::Mat grayscale, imageScaled;

	std::vector<cv::Rect> faces;
	cv::cvtColor(image, grayscale, cv::COLOR_BGR2GRAY);
	cv::resize(grayscale, imageScaled, cv::Size(), scale, scale, cv::INTER_LINEAR);
	cv::equalizeHist(imageScaled, imageScaled);

	const int faceFlags = CV_HAAR_SCALE_IMAGE | CV_HAAR_FIND_BIGGEST_OBJECT;
	cascade.detectMultiScale(imageScaled, faces, 1.2, 2, faceFlags, cv::Size(128, 128), cv::Size(1000, 1000));
	
	cv::Rect face0 = cv::Rect(0, 0, image.cols, image.rows);
	
	if (faces.size() > 0)
	{
		// Rescale detection
		const double rescale = (1.0 / scale);
		const int face0_rescale_x = cvRound(faces[0].x * rescale);
		const int face0_rescale_y = cvRound(faces[0].y * rescale);
		const int face0_rescale_width = cvRound(faces[0].width * rescale);
		const int face0_rescale_height = cvRound(faces[0].height * rescale);
		face0 = cv::Rect(face0_rescale_x, face0_rescale_y, face0_rescale_width, face0_rescale_height);

		// Add margin (OpenCV detection similar to CCV library detection?)
		const int margin = cvRound(0.15 * face0.width);
		const int face0_margin_x = std::max(0, face0.x - margin);
		const int face0_margin_y = std::max(0, face0.y - margin);
		const int face0_margin_width = std::min(image.cols, face0.x + face0.width + 2*margin) - face0.x;
		const int face0_margin_height = std::min(image.rows, face0.y + face0.height + 2*margin) - face0.y;
		face0 = cv::Rect(face0_margin_x, face0_margin_y, face0_margin_width, face0_margin_height);
	}

	return face0;
}
// detectFace


// =======================================================================
// SMOOTH_LABELING
// -----------------------------------------------------------------------
/**
 * Funcion que suaviza las detecciones con un filtro temporal.
 */
// =======================================================================
std::string smoothLabeling(std::vector<std::string> &vecLabels, std::string &newLabel, const int size)
{
	// Insert new label
	vecLabels.push_back(newLabel);
	if (vecLabels.size() > (unsigned int) size)
		vecLabels.erase(vecLabels.begin());

	// Count labels
	std::map<std::string, int> mapLabels;
	for (size_t i=0; i<vecLabels.size(); i++)
	{
		const std::string label = vecLabels[i];
		mapLabels[label] = mapLabels[label] + 1;
	}

	// Sort labels (greater)
	std::vector<std::pair<std::string, int> > vecpairLabels;
	vecpairLabels.assign(mapLabels.begin(), mapLabels.end());
	std::sort(vecpairLabels.begin(), vecpairLabels.end(), greater_second<pair<std::string, int> >());

	// Smooth labels
	const int smoothThr = std::max(1, (size + 1) / 2);
	std::string smoothLabel = "";

	if ((vecpairLabels.size() > 0) && (vecpairLabels[0].second >= smoothThr))
		smoothLabel = vecpairLabels[0].first;

	return smoothLabel;
}
// smoothLabeling



// =======================================================================
// MAIN FUNCTION
// =======================================================================


// =======================================================================
// MAIN
// -----------------------------------------------------------------------
/**
 * Funcion principal del programa.
 */
// =======================================================================
int main(int argc, char* argv[])
{
	std::cout << "facedetect v1.0" << std::endl << std::endl;

	// Parse Command-Line arguments
	std::map<std::string, std::string> parameters;
	commandLineParser(argc, argv, parameters);

	if ((argc == 1) || (parameters[TAG_PARAM_INPUT].empty()))
	{
		std::cout << "Detect faces from video" << std::endl << std::endl;
		std::cout << "facedetect -input=<file> [-output=<file>]" << std::endl;
		std::cout << "     input  Input video filename." << std::endl;
		std::cout << "    output  Input video filename." << std::endl << std::endl;
		return 1;
	}
	const std::string inputFile = parameters[TAG_PARAM_INPUT];
	const std::string outputFile = parameters[TAG_PARAM_OUTPUT];
	const bool saveVideo = !outputFile.empty();


	// -------------------------------------------------------------------

	// Load Caffe library
	const std::string model_file   = "Caffe/model.prototxt";
	const std::string trained_file = "Caffe/trained.caffemodel";
	const std::string mean_file    = "Caffe/mean.binaryproto";
	const std::string label_file   = "Caffe/labels.txt";
	
	Classifier classifier(model_file, trained_file, mean_file, label_file);
	std::vector<Prediction> predictions;
	std::vector<std::string> vecLabels;
	std::string smoothLabel;
	std::string currLabel;
	int labeled = 0;

	std::vector<float> labelsProb(NUM_LABELS, 0.0);
	std::vector<int> labelsHist(NUM_LABELS, 0);
	std::vector<std::string> classifierLabels = classifier.GetLabels();

	std::map<std::string, int> indexLabels;
	for (size_t i=0; i<classifierLabels.size(); i++)
		indexLabels[classifierLabels[i]] = i;


	// -------------------------------------------------------------------

	// Load face detector
	const std::string cascadeName = "Classifiers/haarcascade_frontalface_alt.xml";
	const std::string nestedCascadeName = "Classifiers/haarcascade_eye_tree_eyeglasses.xml";

	cv::CascadeClassifier cascade, nestedCascade;
	const double scale = 1.0;

	if (!cascade.load(cascadeName))
	{
		std::cerr << "ERROR: Could not load classifier cascade" << std::endl;
		return -1;
	}
	if (!nestedCascade.load(nestedCascadeName))
	{
		std::cerr << "WARNING: Could not load classifier cascade for nested objects" << std::endl;
		return -1;
	}


	// -------------------------------------------------------------------

	// Open video source
	cv::VideoCapture capture(inputFile);        // Open the video
	if (!capture.isOpened())                    // Check if we succeeded
		return -1;

	// Write video labeled
	cv::VideoWriter writerVideo;
	if (saveVideo == true)
	{
		writerVideo.open(outputFile, capture.get(CV_CAP_PROP_FOURCC), capture.get(CV_CAP_PROP_FPS),
									 cv::Size(capture.get(CV_CAP_PROP_FRAME_WIDTH), capture.get(CV_CAP_PROP_FRAME_HEIGHT)));
		if (!writerVideo.isOpened())            // Check if we succeeded
			return -1;
	}

	cv::Mat image;
	cv::namedWindow("facedetect");
	std::cout << "Detecting faces from video..." << std::endl;

	while (true)
	{
		// Get frame from video
		capture >> image;

		// Check end of file
		if (image.empty() == true) break;

		// Face detection
		cv::Rect face0 = detectFace(image, cascade, nestedCascade, scale);
		predictions.clear();
		currLabel.clear();

		if ((face0.width != image.cols) && (face0.height != image.rows))
		{
			// Predict emotion
			const cv::Mat image_face0 = image(face0);
			predictions = classifier.Classify(image_face0, NUM_LABELS);
			currLabel = predictions[0].first;
			labeled++;

			// Update information labelsProb
			for (size_t i=0; i<labelsProb.size(); i++)
			{
				const int index = indexLabels[predictions[0].first];
				labelsProb[index] += predictions[i].second;
			}

			// Update information labelsProb
			std::vector<int> bestPrediction(NUM_LABELS, 0);
			const int index = indexLabels[predictions[0].first];
			bestPrediction[index] = 1;
			std::transform(labelsHist.begin(), labelsHist.end(), bestPrediction.begin(), labelsHist.begin(), std::plus<int>());

			// Show face
			cv::Point pointTL = cv::Point(face0.x, face0.y);
			cv::Point pointBR = cv::Point(face0.x + face0.width, face0.y + face0.height);
			cv::rectangle(image, pointTL, pointBR, cv::Scalar(0, 0, 255), 2, 8, 0);
		}

		// Show result
		smoothLabel = smoothLabeling(vecLabels, currLabel, NUM_SMOOTH_LABELS);
		cv::putText(image, smoothLabel.c_str(), cvPoint(10, 30), cv::FONT_HERSHEY_SIMPLEX, 1.0, cv::Scalar(255, 255, 255), 2, CV_AA);
		cv::imshow("facedetect", image);

		// Write frame to video
		if (saveVideo == true)
			writerVideo << image;

		if (cv::waitKey(1) >= 0) 
			break;
	}

	capture.release();
	writerVideo.release();


	// -------------------------------------------------------------------

	// Show information labelsProb
	std::transform(labelsProb.begin(), labelsProb.end(), labelsProb.begin(), std::bind2nd(std::divides<float>(), labeled));

	std::cout << cv::format("\nMean probabilities in %d frames:\n", labeled);
	std::cout << cv::format("%8s - %.6f\n", classifierLabels[0].c_str(), labelsProb[0]);
	std::cout << cv::format("%8s - %.6f\n", classifierLabels[6].c_str(), labelsProb[6]);
	std::cout << cv::format("%8s - %.6f\n", classifierLabels[2].c_str(), labelsProb[2]);
	std::cout << cv::format("%8s - %.6f\n", classifierLabels[7].c_str(), labelsProb[7]);
	std::cout << cv::format("%8s - %.6f\n", classifierLabels[5].c_str(), labelsProb[5]);
	std::cout << cv::format("%8s - %.6f\n", classifierLabels[3].c_str(), labelsProb[3]);
	std::cout << cv::format("%8s - %.6f\n", classifierLabels[4].c_str(), labelsProb[4]);
	std::cout << cv::format("%8s - %.6f\n", classifierLabels[1].c_str(), labelsProb[1]);

	// Show information labelsHist
	std::cout << cv::format("\nHistogram of best predictions:\n");
	std::cout << cv::format("%8s - %d\n", classifierLabels[0].c_str(), labelsHist[0]);
	std::cout << cv::format("%8s - %d\n", classifierLabels[6].c_str(), labelsHist[6]);
	std::cout << cv::format("%8s - %d\n", classifierLabels[2].c_str(), labelsHist[2]);
	std::cout << cv::format("%8s - %d\n", classifierLabels[7].c_str(), labelsHist[7]);
	std::cout << cv::format("%8s - %d\n", classifierLabels[5].c_str(), labelsHist[5]);
	std::cout << cv::format("%8s - %d\n", classifierLabels[3].c_str(), labelsHist[3]);
	std::cout << cv::format("%8s - %d\n", classifierLabels[4].c_str(), labelsHist[4]);
	std::cout << cv::format("%8s - %d\n", classifierLabels[1].c_str(), labelsHist[1]);

	return 0;
}
// main

