//////////////////////////////////////////////////////////////////////////
// MAIN
// -----------------------------------------------------------------------
/**
 * Classe que gestiona la clasificacion de una red neural convolucionada de Caffe
 *
 * @author	Classification Caffe example
 * @adapted	Miguel Vinyas <miguel@cvc.uab.es>
 *
 * 			Centro de Vision por Computador
 * 			Edifici O - Universitat Autonoma de Barcelona.
 * 			08193 Cerdanyola del Valles, Barcelona, (SPAIN).
 *			Tel. +(34) 93.581.18.28
 *			Fax. +(34) 93.581.16.70
 *
 * @version 1.0 (14 de Marzo de 2017)
 */
//////////////////////////////////////////////////////////////////////////
#if !defined(_CLASSIFICATION_HPP__INCLUDED_)
#define _CLASSIFICATION_HPP__INCLUDED_

#include <caffe/caffe.hpp>
#include <opencv2/core/core.hpp>
#include <string>
#include <vector>

//////////////////////////////////////////////////////////////////////////
using namespace caffe;
using std::string;


//////////////////////////////////////////////////////////////////////////
/* Pair (label, confidence) representing a prediction. */
typedef std::pair<string, float> Prediction;


//////////////////////////////////////////////////////////////////////////
class Classifier {
 public:
  Classifier(const string& model_file,
             const string& trained_file,
             const string& mean_file,
             const string& label_file);

  std::vector<std::pair<int, float> > ClassifyIdx(const cv::Mat& img, int N = 5);
  std::vector<Prediction> Classify(const cv::Mat& img, int N = 5);

  std::vector<string> GetLabels();

 private:
  void SetMean(const string& mean_file);

  std::vector<float> Predict(const cv::Mat& img);

  void WrapInputLayer(std::vector<cv::Mat>* input_channels);

  void Preprocess(const cv::Mat& img,
                  std::vector<cv::Mat>* input_channels);

 private:
  shared_ptr<Net<float> > net_;
  cv::Size input_geometry_;
  int num_channels_;
  cv::Mat mean_;
  std::vector<string> labels_;
};

#endif		// #define _CLASSIFICATION_HPP__INCLUDED_
