extern "C" {
    int init(void **cascade_ptr_ptr, void **classifier_ptr_ptr, char ***label_names_ptr);
    void classify(int *output_bbox, int *output_labels, float *output_prs, int label_num,
                  void *cascade_ptr, double scale, void *classifier_ptr,
                  int frame_width, int frame_height, unsigned char *frame_data);
    void fin(void *cascade_ptr, void *classifier_ptr, char **label_names, int label_num);
}