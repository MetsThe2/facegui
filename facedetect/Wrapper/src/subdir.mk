# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/wrapper.cpp 

OBJS += \
./src/wrapper.o 

CPP_DEPS += \
./src/wrapper.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -fPIC -I $$NV_CAFFE_015_ROOT/include -I/usr/local/include -I/usr/local/cuda/include -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


