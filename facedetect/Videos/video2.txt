Mean proabilities in 568 frames:
 neutral - 0.720068
 sadness - 0.000000
contempt - 0.000000
surprise - 0.045775
     joy - 0.216549
 disgust - 0.001761
    fear - 0.008803
   anger - 0.007042

Histogram of best predictions:
 neutral - 409
 sadness - 0
contempt - 0
surprise - 26
     joy - 123
 disgust - 1
    fear - 5
   anger - 4

