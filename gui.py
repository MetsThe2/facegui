#!/usr/bin/env python2

# TODO: hist, smooth label? (in addition to exponential moving average)

import os
import sys
import ctypes as ct
import cv2 as cv
import easygui
from PIL import Image
from easydict import EasyDict as edict
import matplotlib.pyplot as plt
import matplotlib.animation as anim
import numpy as np

max_plot_width_in_inches = 6
default_cam_id = 0
max_cam_id = 10

# --- i/o ---

def get_file_path():
    #return filedialog.askopenfilename(parent=tk_root)
    return easygui.fileopenbox()

def show_msg(msg):
    print(msg) # TODO

def pause(): # For testing
    try:
        input()
    except:
        pass

# --- model ---

def get_cv_model():
    return edict({
        'lib': None,
        'cascade': cv.CascadeClassifier('cascade.xml'),
        'label_names': ('unknown',),
        'label_num': 1
    })

def get_caffe_model():
    lib = None
    try:
        path = '/usr/src/app/facegui/facedetect/Wrapper/wrapper.so'
        lib = ct.CDLL(path)
    except OSError as e:
        print(e)
        print("Error: Missing wrapper.so")
        return None

    cascade, classifier = ct.c_void_p(), ct.c_void_p()
    label_names = ct.POINTER(ct.c_char_p)()
    label_num = lib.init(ct.byref(cascade), ct.byref(classifier), ct.byref(label_names))
    return None if label_num < 0 else edict({
        'lib': lib,
        'cascade': cascade,
        'classifier': classifier,
        'label_names': label_names,
        'label_num': label_num
    })

# --- init ---

def get_logos():
    logos = cv.imread('logos.png')
    if logos is None:
        print("Warning: Couldn't read image file with logos.")
        logos = np.zeros((10, 10, 3), dtype=np.uint8) # Black RGB square
    return logos

def print_help():
    print("\nKeyboard shortcuts:")
    print("q\t-\tquit")
    print("i\t-\tinput file")
    # TODO: print("o\t-\toutput file")
    print("c\t-\tcamera input")
    sys.stdout.flush() # Otherwise the help text might only be printed when the
                       # program is closed, which isn't tremendously helpful...

def init(use_caffe=True):
    print_help()
#    tk_root = tk.Tk()
 #   tk_root.withdraw()
    return get_caffe_model() if use_caffe else get_cv_model(), get_logos() #, tk_root

# cap

def try_open_vid_cap(path_or_id):
    try:
        cap = cv.VideoCapture(path_or_id)
        if cap.isOpened():
            return cap
        else:
            cap.release()
    except Exception as e:
        print(e)
    return None

def get_vid_cap(path_or_id):
    if type(path_or_id) is str:
        return try_open_vid_cap(path_or_id)
    
    cap = try_open_vid_cap(path_or_id)
    if cap is not None:
        return cap

    for cam_id in range(max_cam_id + 1):
        cap = try_open_vid_cap(cam_id)
        if cap is not None:
            return cap
    return None

# --- src ---

def is_im(path):
    try:
        im = Image.open(path)
        im.verify()
        im.close()
    except:
        return False
    return True

def get_src(path_or_id):
    src = None
    if is_im(path_or_id):
        frame = cv.imread(path_or_id)
        if frame is not None:
            src = edict({'frame': frame, 'cap': None})
    else:
        cap = get_vid_cap(path_or_id)
        if cap is not None:
            src = edict({'cap': cap})
    if src is None:
        cap_name = 'webcam' if type(path_or_id) is int else str(path_or_id)
        show_msg("Error: Couldn't open " + cap_name + '.')
    return src

def is_im_src(src):
    return src.cap is None

def read_src(src, frame):
    if is_im_src(src):
        return src.frame.copy()

    ret, new_frame = src.cap.read()
    return new_frame if ret else frame

def close_src(src):
    if is_im_src(src):
        del src.frame
    else:
        src.cap.release()

def try_change_src(src, new_path_or_id):
    if not is_im_src(src):
        close_src(src) # Free up camera if capture is not from video.
    new_src = get_src(new_path_or_id)
    if new_src is not None:
        close_src(src)
        return new_src, True
    return src, False

# --- classify ---

def cv_classify(model, frame):
    face_bboxs = model.cascade.detectMultiScale(
        cv.cvtColor(frame, cv.COLOR_BGR2GRAY),
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30),
        flags=cv.CASCADE_SCALE_IMAGE
    )
    if len(face_bboxs) == 0:
        return None

    labels, prs =(0,), (1.0,)
    return face_bboxs[0], labels, prs

def caffe_classify(model, frame):
    BBox = ct.c_int * 4
    Labels = ct.c_int * model.label_num
    Prs = ct.c_float * model.label_num
    output_bbox = BBox()
    output_labels = Labels()
    output_prs = Prs()
    cascade_ptr = model.cascade
    scale = 1.1
    classifier_ptr = model.classifier
    frame_width = frame.shape[1]
    frame_height = frame.shape[0]
    uchar_p = ct.POINTER(ct.c_ubyte)
    frame_data = frame.ctypes.data_as(uchar_p)
    model.lib.classify.argtypes = (
        BBox, Labels, Prs, ct.c_int,
        ct.c_void_p, ct.c_double, ct.c_void_p,
        ct.c_int, ct.c_int, uchar_p
    )
    model.lib.classify(output_bbox, output_labels, output_prs, model.label_num,
                       cascade_ptr, scale, classifier_ptr,
                       frame_width, frame_height, frame_data)
    if output_labels[0] < 0:
        return None
    return tuple(output_bbox), tuple(output_labels), tuple(output_prs)

def classify(model, frame):
    if model.lib is None:
        return cv_classify(model, frame)
    else:
        return caffe_classify(model, frame)

# --- draw ---

def draw_bbox(frame, bbox):
    x, y, w, h = bbox
    cv.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)

def get_fig_rgb(fig): # OpenCV uses (h, w, c) ordering instead of (w, h, c) for some reason.
    w, h = fig.canvas.get_width_height()
    buf = np.fromstring(fig.canvas.tostring_rgb(), dtype=np.uint8)
    buf.shape = (h, w, 3)
    return buf

def draw_logos(rgb, logos):
    rgb_width,   rgb_height   = rgb.shape[1],   rgb.shape[0]
    logos_width, logos_height = logos.shape[1], logos.shape[0]
    margin = 10
    if rgb_width < logos_width + margin or rgb_height < logos_height + margin:
        print("Warning: Not enough space to draw logos.", logos_width, logos_height)
        return rgb

    x, y = rgb_width - logos_width - 5, 5
    rgb[y:y+logos_height, x:x+logos_width, :] = logos
    return rgb

def draw_classification(frame, stats, logos, output):
    if output is not None:
        face_bbox, labels, prs = output
        draw_bbox(frame, face_bbox)

    frame_height = frame.shape[0]
    if stats.labeled_frame_num % 5 == 1: # Update plot after every fifth frame.
        if stats.bars is None:
            width_in_inches = min(max_plot_width_in_inches, stats.fig.get_figwidth())
            height_in_inches = frame_height / stats.fig.get_dpi()
            stats.fig.set_size_inches(width_in_inches, height_in_inches, forward=True)

            ax, xs = stats.ax, np.arange(stats.label_num)
            bar_width = 0.35
            ax.set_xticks(xs)
            ax.set_xticklabels(stats.label_names, fontsize=8, rotation=0)
            ax.set_ylabel('Average probability')
            plt.ylim((0, 1))
            stats.bars = ax.bar(xs, stats.pr_avgs, bar_width, color='r')
        else:
            for i in range(len(stats.bars)):
                stats.bars[i].set_height(stats.pr_avgs[i])
        stats.fig.canvas.draw()
        stats.rgb = draw_logos(get_fig_rgb(stats.fig), logos)
    
    rgb_height = stats.rgb.shape[0]
    glued_height = min(frame_height, rgb_height) # Usually frame_height == rgb_height
    return np.concatenate((frame[:glued_height,:,:], stats.rgb[:glued_height,:,:]), axis=1)

# --- stats ---

def reset_stats(stats):
    stats.labeled_frame_num = 0
    stats.pr_avgs = [1.0 / stats.label_num] * stats.label_num
    stats.bars = None
    stats.fig, stats.ax = plt.subplots()

def get_stats(model):
    stats = edict({'label_num': model.label_num, 'bars': None, 'rgb': None})
    reset_stats(stats)
    if model.lib is None:
        stats.label_names = model.label_names
    else: # `model.label_names` is C array of char pointers.
        stats.label_names = [None] * model.label_num
        for i in range(model.label_num):
            stats.label_names[i] = str(model.label_names[i])
    return stats

def update_stats(stats, output):
    stats.labeled_frame_num += 1
    if output is None:
        return

    _, labels, prs = output
    label_prs = [None] * stats.label_num
    for i in range(stats.label_num):
        label_prs[labels[i]] = prs[i]

    for i in range(stats.label_num):
        smoothness = 0.98 # Exponential moving average
        stats.pr_avgs[i] = stats.pr_avgs[i] * smoothness + label_prs[i] * (1 - smoothness)

def close_stats(stats):
    plt.close(stats.fig)
    stats.fig, stats.ax = None, None

# --- run ---

def run(model, src_path_or_id, logos):
    src = get_src(src_path_or_id)
    if src is None:
        return False

    frame = None
    stats = get_stats(model)
    first_frame = True
    while True:
        key = cv.waitKey(2) & 0xFF
        changed = False
        if key == ord('q'):
            break
        elif key == ord('c'):
            src, changed = try_change_src(src, default_cam_id)
        elif key == ord('i'):
            src, changed = try_change_src(src, get_file_path())
        elif key == ord('o'):
            pass # output_file = get_file_path()
            # TODO: Write output video with bounding boxes (and labels?).
        if changed:
            reset_stats(stats)

        frame = read_src(src, frame)
        if frame is None:
            show_msg("Error: Couldn't read any frames.")
            break

        output = classify(model, frame)
        update_stats(stats, output)
        cv.imshow('facedetect', draw_classification(frame, stats, logos, output))
    
        if changed or first_frame:
            cv.moveWindow('facedetect', 1, 100)
        first_frame = False
    close_stats(stats)
    close_src(src)
    return True

def fin(model):
    cv.destroyAllWindows()
    if model.lib is not None:
        model.lib.fin(model.cascade, model.classifier, model.label_names, model.label_num)

# --- main ---

use_caffe = len(sys.argv) < 2 or sys.argv[1].strip() != '0'
model, logos = init(use_caffe)
if model is not None:
    if not run(model, sys.argv[2] if len(sys.argv) > 2 else default_cam_id, logos):
        run(model, "pleaseno.png", logos)
    fin(model)
