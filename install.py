#!/usr/bin/env python3.5

# Before running on Ubuntu 14.04:
# sudo add-apt-repository ppa:fkrull/deadsnakes
# sudo apt-get update
# sudo apt-get install -y python3.5


# Install levels:
# -1        Completely uninstalled
#  0        Partially installed/uninstalled (something must be fixed manually)
# +1        Completely installed

import os
import subprocess
from zipfile import PyZipFile

def bash(s):
    print("------", s)
    try:
        # Add keyword argument `encoding='utf8'` with Python 3.6.
        return subprocess.check_output(s, shell=True, stderr=subprocess.STDOUT,
                                       universal_newlines=True)
    except Exception as e:
        print(e)
        print(e.output)
        raise

def extract_url(url, zip_name):
    bash('wget ' + url)
    try:
        PyZipFile(zip_name).extractall()
        os.remove(zip_name)
        return True
    except Exception as e:
        print(e)
        print("Couldn't download and extract " + url)
        return False

def get_ubuntu_version():
    lines = bash('lsb_release -a').strip().split('\n')
    release_line = None
    for line in lines:
        if "Release" in line:
            release_line = line
            break
    if release_line is None:
        print("Error: Couldn't find line with Ubuntu version.")
        return None

    _, version = release_line.split(':')
    return version.strip()

# --- CUDA ---

def nvcc_install_level():
    lines = bash('nvcc --version').strip().split('\n')
    t = lines[-1].split(',')

    if len(t) == 3:
        version = t[2]
        if version[2] == '8':
            return 1

    if 'command not found' in lines[-1]:
        return -1

    print("Log:")
    print(lines)
    print("nvcc hasn't been completely removed.")
    return 0

def cuda_dir_install_level():
    lines = bash('ls /usr/local | grep cuda').strip().split('\n')
    if len(lines) == 0:
        return -1

    if lines[0].strip() in ('cuda', 'cuda-8.0'):
        return 1

    print("Log:")
    print(lines)
    print("CUDA directory hasn't been completely removed.")
    return 0

def cuda_install_level():
    nvcc_level, dir_level = nvcc_install_level(), cuda_dir_install_level()
    if nvcc_level == 1 and dir_level == 1:
        return 1

    if nvcc_level == -1 and dir_level == -1:
        return -1

    print("If CUDA version is not 8.0, it hasn't been uninstalled properly.")
    print("If CUDA version is 8.0, it hasn't been installed properly.")
    return 0

def get_cuda_dir():
    return os.path.join('/usr/local', bash('ls /usr/local | grep cuda').strip().split('\n')[0].strip())

def install_cuda():
    print("Installing CUDA 8.0.")
    bash('''
    wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/cuda-repo-ubuntu1604_8.0.61-1_amd64.deb
    sudo dpkg -i cuda-repo-ubuntu1604_8.0.61-1_amd64.deb
    rm cuda-repo-ubuntu1604_8.0.61-1_amd64.deb
    
    echo 'export CUDA_HOME=/usr/local/cuda
    export CUDA_ROOT=/usr/local/cuda
    export PATH=$PATH:$CUDA_ROOT/bin:$HOME/bin
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$CUDA_ROOT/lib64
    ' >> ~/.bashrc
    
    source ~/.bashrc

    sudo apt-get update 
    sudo apt-get install -y cuda
    ''')

def maybe_install_cuda():
    cuda_level = cuda_install_level()
    if cuda_level == 0:
        return False # Failure

    if cuda_level == -1:
        install_cuda()
        cuda_level = cuda_install_level()
    
    assert cuda_level == 1, cuda_level
    return True # Success

# --- OpenCV ---

def install_opencv_deps():
    print("Installing OpenCV 3 dependencies.")
    bash('''
    sudo apt-get update
    sudo apt-get install --assume-yes --no-install-recommends wget
    sudo apt-get install --assume-yes build-essential cmake git
    sudo apt-get install --assume-yes pkg-config unzip ffmpeg qtbase5-dev python-dev python3-dev python-numpy python3-numpy
    sudo apt-get install --assume-yes libopencv-dev libgtk-3-dev libdc1394-22 libdc1394-22-dev libjpeg-dev libpng12-dev libtiff5-dev libjasper-dev
    sudo apt-get install --assume-yes libavcodec-dev libavformat-dev libswscale-dev libxine2-dev libgstreamer0.10-dev libgstreamer-plugins-base0.10-dev
    sudo apt-get install --assume-yes libv4l-dev libtbb-dev libfaac-dev libmp3lame-dev libopencore-amrnb-dev libopencore-amrwb-dev libtheora-dev
    sudo apt-get install --assume-yes libvorbis-dev libxvidcore-dev v4l-utils vtk6
    sudo apt-get install --assume-yes liblapacke-dev libopenblas-dev libgdal-dev checkinstall
    ''')

def opencv_install_level():
    try:
        lines = bash('ls /usr/local/lib | grep libopencv_core.so.3.3').strip().split('\n')
    except:
        return 0

    if 'libopencv_core.so.3.3' in lines[0]:
        return 1

    return 0 # TODO: Reliable way to detect that OpenCV is not installed (install level -1)

def install_opencv():
    print("Installing OpenCV 3.3.1.")
    dir_name = '3.3.1'
    zip_name = dir_name + '.zip'
    if not extract_url('https://github.com/opencv/opencv/archive/' + zip_name, zip_name):
        return False

    bash('''
    cd ''' + dir_name + '''
    mkdir build
    cd build/
    cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D FORCE_VTK=ON -D WITH_TBB=ON -D WITH_V4L=ON -D WITH_QT=ON -D WITH_OPENGL=ON -D WITH_CUBLAS=ON -D CUDA_NVCC_FLAGS="-D_FORCE_INLINES" -D WITH_GDAL=ON -D WITH_XINE=ON -D BUILD_EXAMPLES=ON ..
    make -j $(($(nproc) + 1))
    sudo make install
    sudo /bin/bash -c 'echo "/usr/local/lib" > /etc/ld.so.conf.d/opencv.conf'
    sudo ldconfig
    sudo apt-get update
    ''')
    print("If installation fails after this, try restarting Ubuntu.")

def maybe_install_opencv():
    opencv_level = opencv_install_level()
    if opencv_level == 1:
        return True

    print("OpenCV 3.3 does not appear to be installed.")
    print("If it is not installed, enter y to install it:")
    s = input()
    if s.strip() == 'y':
        install_opencv()
        opencv_level = opencv_install_level()
        assert opencv_level == 1, opencv_level
    else:
        print("Assuming OpenCV 3.3 is installed.")
    return True

# --- Caffe ---

def write_caffe_make_config(caffe_dir, cuda_dir):
    s =  r'''
    OPENCV_VERSION := 3
    CUDA_DIR := ''' + cuda_dir + '''
    CUDA_ARCH := -gencode arch=compute_20,code=sm_20 \
                 -gencode arch=compute_20,code=sm_21 \
                 -gencode arch=compute_30,code=sm_30 \
                 -gencode arch=compute_35,code=sm_35 \
                 -gencode arch=compute_50,code=sm_50 \
                 -gencode arch=compute_50,code=compute_50
    BLAS := atlas # 'mkl' for MKL, 'open' for OpenBlas
    INCLUDE_DIRS := $(PYTHON_INCLUDE) /usr/local/include /usr/include/hdf5/serial
    LIBRARY_DIRS := $(PYTHON_LIB) /usr/local/lib /usr/lib /usr/lib/x86_64-linux-gnu/hdf5/serial \
                    /usr/local/share/OpenCV/3rdparty/lib
    BUILD_DIR := build
    DISTRIBUTE_DIR := distribute
    TEST_GPUID := 0 # The ID of the GPU that 'make runtest' will use to run unit tests.
    Q ?= @ # enable pretty build (comment to see full commands)
    LIBRARY_NAME_SUFFIX := -nv # shared object suffix name to differentiate branches
    '''
    with open(os.path.join(caffe_dir, 'Makefile.config'), 'w') as f:
        f.write(s)

def install_caffe_deps():
    print("Installing Caffe dependencies.")
    bash('''
    sudo apt-get install -y libprotobuf-dev libleveldb-dev libsnappy-dev libopencv-dev libhdf5-serial-dev protobuf-compiler
    sudo apt-get install -y --no-install-recommends libboost-all-dev
    sudo apt-get install -y libatlas-base-dev
    sudo apt-get install -y libgflags-dev libgoogle-glog-dev liblmdb-dev
    ''')

def install_caffe(dir_name):
    print("Installing NVIDIA Caffe 0.15.")
    install_caffe_deps()
    if not extract_url('https://github.com/NVIDIA/caffe/archive/caffe-0.15.zip', 'caffe-0.15.zip'):
        return False

    write_caffe_make_config(dir_name, get_cuda_dir())
    bash('cd ' + dir_name + '; make clean; make all -j $(($(nproc) + 1))')
    #bash('cd ' + dir_name + '; make test; make runtest') # I like to live dangerously.
    bash('export NV_CAFFE_015_ROOT=' + os.path.abspath(dir_name))
    return True

def maybe_install_caffe():
    dir_name = 'caffe-caffe-0.15'
    return os.path.exists(dir_name) or install_caffe(dir_name)

# --- facegui ---

def make_wrapper():
    bash('cd facedetect/Wrapper; make clean; make all')

def pip_install_deps():
    bash('''
    pip2 install --user "opencv-python>=3.3,<3.4"
    pip2 install --user "matplotlib>=2,<3"
    pip2 install --user pillow
    pip2 install --user easygui
    pip2 install --user easydict
    ''')

def install_facegui():
    ubuntu_version = get_ubuntu_version()
    if ubuntu_version not in ["14.04", "16.04"]:
        print("Error: Ubuntu version " + str(ubuntu_version) + " is not supported.")
        return False

    install_opencv_deps()
    if not (maybe_install_cuda() and maybe_install_opencv() and maybe_install_caffe()):
        return False

    make_wrapper()
    pip_install_deps()
    return True

# --- main ---

if __name__ == '__main__':
    print("Done" if install_facegui() else "Couldn't install facegui.")
